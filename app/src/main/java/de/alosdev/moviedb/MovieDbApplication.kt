package de.alosdev.moviedb

import android.app.Application
import de.alosdev.moviedb.config.configModule
import de.alosdev.moviedb.core.coreModule
import de.alosdev.moviedb.http.httpModule
import de.alosdev.moviedb.movie.list.movieListModules
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class MovieDbApplication : Application() {

    internal val moduleList = setOf(
        coreModule,
        httpModule,
        configModule)
        .plus(movieListModules)

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin(this, moduleList.toList())
    }
}
