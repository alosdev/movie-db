package de.alosdev.moviedb.config

import de.alosdev.moviedb.config.api.ConfigApi
import de.alosdev.moviedb.config.repository.ConfigRepository
import org.koin.dsl.module.module
import retrofit2.Retrofit

val configModule = module {
    factory {
        ConfigRepository(
            dispatchers = get(),
            api = createConfigApi(get())
        )
    }
}

private fun createConfigApi(retrofit: Retrofit): ConfigApi {
    return retrofit.create(ConfigApi::class.java)
}

