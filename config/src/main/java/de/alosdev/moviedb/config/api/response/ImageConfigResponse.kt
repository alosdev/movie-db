package de.alosdev.moviedb.config.api.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true   )
data class ImageConfigResponse(
    @Json(name = "secure_base_url") val secureBaseUrl: String,
    @Json(name = "poster_sizes") val posterSizes: List<String>,
    @Json(name = "backdrop_sizes") val backdropSizes: List<String>
)
