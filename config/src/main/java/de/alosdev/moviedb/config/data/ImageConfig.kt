package de.alosdev.moviedb.config.data

class ImageConfig(
    private val baseUrl: String,
    private val posterSize: String,
    private val backdropSize: String
) {
    fun createPosterImage(image: String): String = "$baseUrl$posterSize/$image"
    fun createBackdropImage(image: String): String = "$baseUrl$backdropSize/$image"
}
