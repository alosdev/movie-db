package de.alosdev.moviedb.config.repository

import de.alosdev.moviedb.config.api.ConfigApi
import de.alosdev.moviedb.config.data.ImageConfig
import de.alosdev.moviedb.core.coroutine.AppCoroutineDispatchers
import kotlinx.coroutines.experimental.withContext

class ConfigRepository(
    private val dispatchers: AppCoroutineDispatchers,
    private val api: ConfigApi
) {
    private lateinit var imageConfig: ImageConfig

    suspend fun fetchImageConfig(): ImageConfig {
        return withContext(dispatchers.network) {
            if (::imageConfig.isInitialized) {
                imageConfig
            } else {
                val response = api.fetchConfigurations().await()
                if (response.isSuccessful) {
                    val configResponse = response.body()!!
                    imageConfig = ImageConfig(
                        configResponse.images.secureBaseUrl,
                        configResponse.images.posterSizes.last(),
                        configResponse.images.backdropSizes.last()
                    )
                    imageConfig
                } else {
                    throw IllegalStateException("couldn't load config")
                }
            }
        }
    }
}
