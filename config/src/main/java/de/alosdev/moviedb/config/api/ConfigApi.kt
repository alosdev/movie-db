package de.alosdev.moviedb.config.api

import de.alosdev.moviedb.config.api.response.ConfigResponse
import kotlinx.coroutines.experimental.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface ConfigApi{
    @GET("configuration")
    fun fetchConfigurations(): Deferred<Response<ConfigResponse>>
}
