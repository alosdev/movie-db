package de.alosdev.moviedb.config.api.response

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ConfigResponse(val images: ImageConfigResponse)
