package de.alosdev.moviedb.config.repository

import com.google.common.truth.Truth.assertThat
import de.alosdev.moviedb.config.api.ConfigApi
import de.alosdev.moviedb.config.api.response.ConfigResponse
import de.alosdev.moviedb.config.api.response.ImageConfigResponse
import de.alosdev.moviedb.core.coroutine.AppCoroutineDispatchers
import kotlinx.coroutines.experimental.Unconfined
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class ConfigRepositoryTest {

    @get:Rule
    val thrown: ExpectedException = ExpectedException.none()

    @Mock
    private lateinit var api: ConfigApi

    private lateinit var toBeTested: ConfigRepository

    @Before
    fun setUp() {
        toBeTested = ConfigRepository(dispatchers, api)
    }

    @Test
    fun checkSuccessfulNetworkCallIfNotInitialized() {
        runBlocking {
            given(api.fetchConfigurations()).willReturn(async { Response.success(createConfigResponse()) })

            val imageConfig = toBeTested.fetchImageConfig()
            assertThat(imageConfig.createPosterImage("path")).isEqualTo("secureUrl/posterOriginal/path")
            assertThat(imageConfig.createBackdropImage("path")).isEqualTo("secureUrl/backdropOriginal/path")
        }
    }

    @Test
    fun checkThrowsIllegalArgumentExceptionWhenConfigFailed() {
        runBlocking {
            given(api.fetchConfigurations()).willReturn(async(Unconfined) { Response.error<ConfigResponse>(404, ResponseBody.create(null, byteArrayOf(0))) })
            thrown.expect(IllegalStateException::class.java)

            toBeTested.fetchImageConfig()
        }
    }
}

internal val dispatchers = AppCoroutineDispatchers(Unconfined, Unconfined, Unconfined)

fun createConfigResponse(): ConfigResponse {
    return ConfigResponse(
        ImageConfigResponse(
            "secureUrl/",
            listOf("posterSmall", "posterOriginal"),
            listOf("backdropSmall", "backdropOriginal")
        )
    )
}
