package de.alosdev.moviedb.movie.detail.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import de.alosdev.moviedb.config.data.ImageConfig
import de.alosdev.moviedb.config.repository.ConfigRepository
import de.alosdev.moviedb.http.data.Failure
import de.alosdev.moviedb.http.data.Success
import de.alosdev.moviedb.movie.TestUtils
import de.alosdev.moviedb.movie.deferred
import de.alosdev.moviedb.movie.detail.api.MovieDetailApi
import de.alosdev.moviedb.movie.detail.api.response.MovieResponse
import de.alosdev.moviedb.movie.detail.data.Movie
import de.alosdev.moviedb.movie.detail.data.mapper.MovieMapper
import kotlinx.coroutines.experimental.runBlocking
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class MovieDetailRepositoryTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var api: MovieDetailApi
    @Mock
    private lateinit var configRepository: ConfigRepository

    private lateinit var toBeTested: MovieDetailRepository

    @Before
    fun setUp() {
        toBeTested = MovieDetailRepository(
            TestUtils.dispatchers,
            api,
            configRepository,
            MovieMapper(),
            4711
        )
    }

    @Test
    fun checkMovieLoadingSuccessful() {
        runBlocking {
            given(api.fetchMovie(4711)).willReturn(deferred(Response.success(MovieResponse(4711, "poster", "backdrop", "title", "overview"))))
            given(configRepository.fetchImageConfig()).willReturn(ImageConfig("url/", "ps", "bs"))

            toBeTested.loadMovie()

            assertThat(toBeTested.movieResource.value).isEqualTo(Success(Movie(4711, "url/ps/poster", "title", "url/bs/backdrop", "overview")))
        }
    }

    @Test
    fun checkMovieFailedWithExceptionWhileLoading() {
        runBlocking {
            given(api.fetchMovie(4711)).willThrow(RuntimeException())
            given(configRepository.fetchImageConfig()).willReturn(ImageConfig("url/", "ps", "bs"))

            toBeTested.loadMovie()

            assertThat(toBeTested.movieResource.value).isEqualTo(Failure(-1, "cannot load movie list"))
        }
    }

    @Test
    fun checkMovieFailedWithRequestNotSuccessful() {
        runBlocking {
            given(api.fetchMovie(4711)).willReturn(deferred(Response.error<MovieResponse>(404, ResponseBody.create(null, byteArrayOf(0)))))
            given(configRepository.fetchImageConfig()).willReturn(ImageConfig("url/", "ps", "bs"))

            toBeTested.loadMovie()

            assertThat(toBeTested.movieResource.value).isEqualTo(Failure(404, "Response.error()"))
        }
    }
}
