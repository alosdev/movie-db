package de.alosdev.moviedb.movie.list.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.google.common.truth.Truth.assertThat
import de.alosdev.moviedb.http.data.Resource
import de.alosdev.moviedb.http.data.Success
import de.alosdev.moviedb.movie.TestUtils
import de.alosdev.moviedb.movie.list.data.Movie
import de.alosdev.moviedb.movie.list.data.MovieList
import de.alosdev.moviedb.movie.list.repository.MovieListRepository
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MovieListViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var repository: MovieListRepository

    private lateinit var toBeTested: MovieListViewModel

    private val resource = MutableLiveData<Resource<MovieList>>()

    @Before
    fun setUp() {
        given(repository.movieResource).willReturn(resource)
        toBeTested = MovieListViewModel(TestUtils.dispatchers, repository)
    }

    @Test
    fun checkLoadFirstPage() {
        runBlocking {

            toBeTested.start()

            then(repository).should().loadFirstPage()
        }
    }

    @Test
    fun checkLoadNextPage() {
        runBlocking {

            toBeTested.loadNextPage()

            then(repository).should().loadNextPage()
        }
    }

    @Test
    fun checkItemSelected() {
        runBlocking {
            toBeTested.movieList.observe(TestUtils.testLifecycleOwner, Observer {  })
            val movie = Movie(4711, "poster", "title")
            resource.postValue(Success(MovieList(1, 1, listOf(movie))))

            toBeTested.onItemClick(0)

            assertThat(toBeTested.action.value).isEqualTo(MovieListViewModel.MovieSelected(movie))
        }
    }
}
