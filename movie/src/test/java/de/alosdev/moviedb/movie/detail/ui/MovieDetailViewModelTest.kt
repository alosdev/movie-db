package de.alosdev.moviedb.movie.detail.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import de.alosdev.moviedb.movie.TestUtils
import de.alosdev.moviedb.movie.detail.repository.MovieDetailRepository
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MovieDetailViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var repository: MovieDetailRepository

    private lateinit var toBeTested: MovieDetailViewModel

    @Before
    fun setUp() {
        given(repository.movieResource).willReturn(MutableLiveData())
        toBeTested = MovieDetailViewModel(TestUtils.dispatchers, repository, "title")
    }

    @Test
    fun checkIfRepositoryGetCalled() {
        runBlocking {
            toBeTested.start()

            then(repository).should().loadMovie()
        }
    }
}
