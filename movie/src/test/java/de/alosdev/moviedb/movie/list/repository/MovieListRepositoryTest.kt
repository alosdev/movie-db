package de.alosdev.moviedb.movie.list.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import de.alosdev.moviedb.config.data.ImageConfig
import de.alosdev.moviedb.config.repository.ConfigRepository
import de.alosdev.moviedb.http.data.Failure
import de.alosdev.moviedb.http.data.Success
import de.alosdev.moviedb.movie.TestUtils
import de.alosdev.moviedb.movie.deferred
import de.alosdev.moviedb.movie.list.api.MovieListApi
import de.alosdev.moviedb.movie.list.api.response.MoviePageResponse
import de.alosdev.moviedb.movie.list.api.response.MovieResponse
import de.alosdev.moviedb.movie.list.data.Movie
import de.alosdev.moviedb.movie.list.data.MovieList
import de.alosdev.moviedb.movie.list.data.mapper.MovieListMapper
import kotlinx.coroutines.experimental.runBlocking
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class MovieListRepositoryTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var api: MovieListApi
    @Mock
    private lateinit var configRepository: ConfigRepository

    private lateinit var toBeTested: MovieListRepository

    @Before
    fun setUp() {
        toBeTested = MovieListRepository(
            TestUtils.dispatchers,
            api,
            configRepository,
            MovieListMapper()
        )
    }

    @Test
    fun checkMovieLoadingSuccessful() {
        runBlocking {
            given(api.fetchPage()).willReturn(deferred(Response.success(MoviePageResponse(1, 2, listOf(MovieResponse(4711, "poster", "title"))))))
            given(configRepository.fetchImageConfig()).willReturn(ImageConfig("url/", "ps", "bs"))

            toBeTested.loadFirstPage()

            assertThat(toBeTested.movieResource.value).isEqualTo(Success(MovieList(1, 2, listOf(Movie(4711, "url/ps/poster", "title")))))
        }
    }

    @Test
    fun checkMovieFailedWithExceptionWhileLoading() {
        runBlocking {
            given(api.fetchPage()).willThrow(RuntimeException())
            given(configRepository.fetchImageConfig()).willReturn(ImageConfig("url/", "ps", "bs"))

            toBeTested.loadFirstPage()

            assertThat(toBeTested.movieResource.value).isEqualTo(Failure(-1, "cannot load movie list"))
        }
    }

    @Test
    fun checkMovieFailedWithRequestNotSuccessful() {
        runBlocking {
            given(api.fetchPage()).willReturn(deferred(Response.error<MoviePageResponse>(404, ResponseBody.create(null, byteArrayOf(0)))))
            given(configRepository.fetchImageConfig()).willReturn(ImageConfig("url/", "ps", "bs"))

            toBeTested.loadFirstPage()

            assertThat(toBeTested.movieResource.value).isEqualTo(Failure(404, "Response.error()"))
        }
    }

    @Test
    fun checkLoadingNextPage() {
        runBlocking {
            toBeTested.movieResource.value = Success(MovieList(1, 2, listOf(Movie(815, "url/ps/poster", "title"))))
            given(api.fetchPage(2)).willReturn(deferred(Response.success(MoviePageResponse(2, 2, listOf(MovieResponse(4711, "poster", "title"))))))
            given(configRepository.fetchImageConfig()).willReturn(ImageConfig("url/", "ps", "bs"))

            toBeTested.loadNextPage()

            assertThat(toBeTested.movieResource.value).isEqualTo(Success(MovieList(2, 2, listOf(Movie(815, "url/ps/poster", "title"), Movie(4711, "url/ps/poster", "title")))))
        }
    }
}
