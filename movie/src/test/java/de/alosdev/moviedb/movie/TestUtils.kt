package de.alosdev.moviedb.movie

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import de.alosdev.moviedb.core.coroutine.AppCoroutineDispatchers
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.Unconfined
import kotlinx.coroutines.experimental.async

object TestUtils {
    val testLifecycleOwner: LifecycleOwner = object : LifecycleOwner {

        private val registry = init()

        // Creates a LifecycleRegistry in RESUMED state.
        private fun init(): LifecycleRegistry {
            val registry = LifecycleRegistry(this)
            registry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
            registry.handleLifecycleEvent(Lifecycle.Event.ON_START)
            registry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
            return registry
        }

        override fun getLifecycle(): Lifecycle {
            return registry
        }
    }

    val dispatchers = AppCoroutineDispatchers(Unconfined, Unconfined, Unconfined)
}

fun <T> deferred(deferred: T): Deferred<T> {
    return async(Unconfined) { deferred }
}
