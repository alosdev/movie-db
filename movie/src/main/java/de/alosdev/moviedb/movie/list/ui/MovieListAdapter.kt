package de.alosdev.moviedb.movie.list.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import de.alosdev.moviedb.core.ui.mvvm.loadImage
import de.alosdev.moviedb.movie.R
import de.alosdev.moviedb.movie.list.data.Movie
import timber.log.Timber

class MovieListAdapter(private val listener: RecyclerViewClickListener) : ListAdapter<Movie, MovieViewHolder>(MovieDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.movie_list_item, parent, false),
            listener
        )
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class MovieViewHolder(view: View, private val listener: RecyclerViewClickListener) : RecyclerView.ViewHolder(view), View.OnClickListener {
    override fun onClick(v: View?) {
        listener.onClick(adapterPosition)
    }

    fun bind(item: Movie) {
        itemView.findViewById<TextView>(R.id.title).text = item.title
        loadImage(itemView.findViewById(R.id.movie_list_image), item.posterPath)
        Timber.d("image %s", item.posterPath)
        itemView.setOnClickListener(this@MovieViewHolder)
    }
}

class MovieDiff : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }
}
