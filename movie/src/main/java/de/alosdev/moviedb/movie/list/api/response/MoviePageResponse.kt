package de.alosdev.moviedb.movie.list.api.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MoviePageResponse(
    val page: Int,
    @Json(name = "total_pages")
    val totalPages: Int,
    val results: List<MovieResponse>
)
