package de.alosdev.moviedb.movie.detail.ui

import android.os.Bundle
import de.alosdev.moviedb.core.ui.ToolbarProvider
import de.alosdev.moviedb.core.ui.mvvm.MvvmFragment
import de.alosdev.moviedb.movie.R
import de.alosdev.moviedb.movie.databinding.MovieDetailFragmentBinding
import org.koin.core.parameter.ParameterDefinition
import org.koin.core.parameter.parametersOf

class MovieDetailFragment : MvvmFragment<MovieDetailFragmentBinding, MovieDetailViewModel>(MovieDetailViewModel::class) {

    override fun getParameters(): ParameterDefinition {
        return {
            val args = MovieDetailFragmentArgs.fromBundle(arguments)
            parametersOf(args.id, args.title)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val parent = activity
        if (parent is ToolbarProvider) parent.getToolbar().title = viewModel.title
    }

    override fun getLayoutId(): Int = R.layout.movie_detail_fragment
}
