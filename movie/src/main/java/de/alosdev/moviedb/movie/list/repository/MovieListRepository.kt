package de.alosdev.moviedb.movie.list.repository

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import de.alosdev.moviedb.config.repository.ConfigRepository
import de.alosdev.moviedb.core.coroutine.AppCoroutineDispatchers
import de.alosdev.moviedb.http.data.Failure
import de.alosdev.moviedb.http.data.Loading
import de.alosdev.moviedb.http.data.Resource
import de.alosdev.moviedb.http.data.Success
import de.alosdev.moviedb.movie.list.api.MovieListApi
import de.alosdev.moviedb.movie.list.data.MovieList
import de.alosdev.moviedb.movie.list.data.mapper.MovieListMapper
import kotlinx.coroutines.experimental.withContext
import timber.log.Timber

class MovieListRepository(
    private val dispatchers: AppCoroutineDispatchers,
    private val api: MovieListApi,
    private val configRepository: ConfigRepository,
    private val mapper: MovieListMapper
) {
    @VisibleForTesting
    internal val resource = MutableLiveData<Resource<MovieList>>()
    val movieResource = resource
    private var dateFilter: String? = null

    suspend fun loadFirstPage() {
        withContext(dispatchers.network) {
            resource.postValue(Loading)
            try {
                val imageConfig = configRepository.fetchImageConfig()
                val response = api.fetchPage(dateFilter = dateFilter).await()
                if (response.isSuccessful) {
                    resource.postValue(
                        Success(
                            mapper.map(
                                imageConfig = imageConfig,
                                pageResponse = response.body()!!
                            )
                        )
                    )
                } else {
                    resource.postValue(Failure(response.code(), response.message()))
                }
            } catch (exception: Exception) {
                Timber.e(exception, "cannot load movie list")
                resource.postValue(Failure(-1, "cannot load movie list"))
            }
        }
    }

    suspend fun loadNextPage() {
        withContext(dispatchers.network) {
            val oldResource = resource.value as Success
            val oldData = oldResource.data
            val imageConfig = configRepository.fetchImageConfig()
            val response = api.fetchPage(page = oldData.currentPage + 1, dateFilter = dateFilter).await()
            if (response.isSuccessful) {
                resource.postValue(
                    Success(
                        oldData.copyAndPushPage(
                            mapper.map(
                                imageConfig = imageConfig,
                                pageResponse = response.body()!!
                            )
                        )
                    )
                )
            }
        }
    }

    suspend fun setDateFilter(year: Int, month: Int, day: Int) {
        dateFilter = "$year-${month + 1}-$day"
        Timber.d(dateFilter)

        loadFirstPage()
    }

    suspend fun resetDateFilter() {
        dateFilter = null
        loadFirstPage()
    }
}
