package de.alosdev.moviedb.movie.list.api

import de.alosdev.moviedb.movie.list.api.response.MoviePageResponse
import kotlinx.coroutines.experimental.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieListApi {
    @GET("discover/movie")
    fun fetchPage(
        //  the page index starts with 1
        @Query("page") page: Int = 1,
        @Query("sort_by") sorting: String = "primary_release_date.desc",
        // date format "year-month-day"
        @Query("release_date.lte") dateFilter: String? = null): Deferred<Response<MoviePageResponse>>
}
