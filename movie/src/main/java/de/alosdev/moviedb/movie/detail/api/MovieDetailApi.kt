package de.alosdev.moviedb.movie.detail.api

import de.alosdev.moviedb.movie.detail.api.response.MovieResponse
import kotlinx.coroutines.experimental.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieDetailApi {
    @GET("movie/{movie_id}?append_to_response=videos,images")
    fun fetchMovie(
        @Path("movie_id") id: Int): Deferred<Response<MovieResponse>>
}
