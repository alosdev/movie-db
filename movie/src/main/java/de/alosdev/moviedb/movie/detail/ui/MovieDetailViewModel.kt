package de.alosdev.moviedb.movie.detail.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations.map
import de.alosdev.moviedb.core.coroutine.AppCoroutineDispatchers
import de.alosdev.moviedb.core.ui.BaseViewModel
import de.alosdev.moviedb.http.data.Loading
import de.alosdev.moviedb.http.data.Success
import de.alosdev.moviedb.movie.detail.data.Movie
import de.alosdev.moviedb.movie.detail.repository.MovieDetailRepository
import kotlinx.coroutines.experimental.withContext

class MovieDetailViewModel(
    private val dispatchers: AppCoroutineDispatchers,
    private val repository: MovieDetailRepository,
    val title: String
) : BaseViewModel(dispatchers) {
    private val resource = repository.movieResource
    val showProgress: LiveData<Boolean> = map(resource) {
        it is Loading
    }
    val showContent: LiveData<Boolean> = map(resource) {
        it is Success
    }
    val movie: LiveData<Movie> = de.alosdev.moviedb.core.ui.livedata.map(resource, { it is Success }) {
        (it as Success).data
    }

    override fun start() {
        launch {
            withContext(dispatchers.computation) {
                repository.loadMovie()
            }
        }
    }
}
