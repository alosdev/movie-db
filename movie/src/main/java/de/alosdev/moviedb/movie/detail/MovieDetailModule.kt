package de.alosdev.moviedb.movie.detail

import de.alosdev.moviedb.movie.detail.api.MovieDetailApi
import de.alosdev.moviedb.movie.detail.data.mapper.MovieMapper
import de.alosdev.moviedb.movie.detail.repository.MovieDetailRepository
import de.alosdev.moviedb.movie.detail.ui.MovieDetailViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module.module
import retrofit2.Retrofit

val movieDetailModule = module {
    viewModel { (id: Int, title: String) ->
        MovieDetailViewModel(
            dispatchers = get(),
            repository = get(
                parameters = { parametersOf(id) }
            ),
            title = title
        )
    }
    factory { (id: Int) ->
        MovieDetailRepository(
            dispatchers = get(),
            api = createMovieListApi(get()),
            mapper = MovieMapper(),
            configRepository = get(),
            movieId = id
        )
    }
}

private fun createMovieListApi(retrofit: Retrofit): MovieDetailApi {
    return retrofit.create(MovieDetailApi::class.java)
}
