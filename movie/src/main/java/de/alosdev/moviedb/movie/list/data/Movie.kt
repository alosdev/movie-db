package de.alosdev.moviedb.movie.list.data

data class Movie(
    val id: Int,
    val posterPath: String? = null,
    val title: String
)
