package de.alosdev.moviedb.movie.list.data

data class MovieList(
    val currentPage: Int,
    private val totalPages: Int,
    val items: List<Movie>
) {
    val hasMorePages = currentPage < totalPages

    fun copyAndPushPage(movieList: MovieList): MovieList {
        val newItems: MutableList<Movie> = items.toMutableList()
        newItems.addAll(movieList.items)
        return MovieList(movieList.currentPage, movieList.totalPages, newItems)
    }
}
