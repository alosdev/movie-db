package de.alosdev.moviedb.movie.list.ui

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import de.alosdev.moviedb.core.coroutine.AppCoroutineDispatchers
import de.alosdev.moviedb.core.ui.BaseViewModel
import de.alosdev.moviedb.core.ui.livedata.map
import de.alosdev.moviedb.core.ui.mvvm.SingleLiveEvent
import de.alosdev.moviedb.core.ui.mvvm.ViewAction
import de.alosdev.moviedb.http.data.Loading
import de.alosdev.moviedb.http.data.Success
import de.alosdev.moviedb.movie.list.data.Movie
import de.alosdev.moviedb.movie.list.repository.MovieListRepository
import kotlinx.coroutines.experimental.withContext
import timber.log.Timber
import androidx.lifecycle.Transformations.map as pureMap

class MovieListViewModel(
    private val dispatchers: AppCoroutineDispatchers,
    private val repository: MovieListRepository
) : BaseViewModel(dispatchers) {
    private val resource = repository.movieResource
    val showProgress: LiveData<Boolean> = pureMap(resource) {
        it is Loading
    }
    val showContent: LiveData<Boolean> = pureMap(resource) {
        it is Success
    }
    val movieList = map(resource, { it is Success }) {
        (it as Success).data
    }
    @VisibleForTesting
    internal var isNotLoading = true

    val action = SingleLiveEvent<ViewAction>()

    override fun start() {
        launch {
            withContext(dispatchers.computation) {
                repository.loadFirstPage()
            }
        }
    }

    fun onItemClick(position: Int) {
        action.postValue(MovieSelected(movieList.value!!.items[position]))
    }

    fun loadNextPage() {
        launch {
            try {
                if (resource.value !is Loading && isNotLoading) {
                    isNotLoading = false
                    repository.loadNextPage()
                }
            } finally {
                isNotLoading = true
            }
        }
    }

    fun setDateFilter(year: Int, month: Int, day: Int) {
        launch{
            Timber.d("set date filter")
            repository.setDateFilter(year, month, day)
        }
    }

    fun resetDateFilter() {
        launch{
            Timber.d("reset date filter")
            repository.resetDateFilter()
        }
    }

    data class MovieSelected(val movie: Movie) : ViewAction()
}
