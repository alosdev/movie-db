package de.alosdev.moviedb.movie.list.api.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieResponse(
    val id: Int,
    @Json(name = "poster_path")
    val posterPath: String? = null,
    val title: String
)
