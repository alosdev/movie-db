package de.alosdev.moviedb.movie.list

import de.alosdev.moviedb.movie.detail.movieDetailModule
import de.alosdev.moviedb.movie.list.api.MovieListApi
import de.alosdev.moviedb.movie.list.data.mapper.MovieListMapper
import de.alosdev.moviedb.movie.list.repository.MovieListRepository
import de.alosdev.moviedb.movie.list.ui.MovieListViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit

val movieListModule = module {
    viewModel {
        MovieListViewModel(
            dispatchers = get(),
            repository = get()
        )
    }
    factory {
        MovieListRepository(
            dispatchers = get(),
            api = createListMovieApi(get()),
            mapper = MovieListMapper(),
            configRepository = get()
        )
    }
}

val movieListModules = listOf(movieListModule, movieDetailModule)

private fun createListMovieApi(retrofit: Retrofit): MovieListApi {
    return retrofit.create(MovieListApi::class.java)
}
