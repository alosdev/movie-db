package de.alosdev.moviedb.movie.list.ui

interface RecyclerViewClickListener {
    fun onClick(position: Int)
}
