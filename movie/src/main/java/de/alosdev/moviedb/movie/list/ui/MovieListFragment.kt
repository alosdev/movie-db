package de.alosdev.moviedb.movie.list.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.alosdev.moviedb.core.ui.mvvm.MvvmFragment
import de.alosdev.moviedb.movie.R
import de.alosdev.moviedb.movie.databinding.MovieListFragmentBinding
import de.alosdev.moviedb.movie.list.ui.MovieListFragmentDirections.actionMovieDetail
import timber.log.Timber

class MovieListFragment : MvvmFragment<MovieListFragmentBinding, MovieListViewModel>(MovieListViewModel::class), DatePickerFragment.DateChangedListener {
    private val scrollListener = PagingListener()

    override fun getLayoutId(): Int = R.layout.movie_list_fragment

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)

        initRecyclerView()

        viewModel.action.observe(this, Observer {
            when (it) {
                is MovieListViewModel.MovieSelected -> {
                    val action = actionMovieDetail(it.movie.id, it.movie.title)
                    findNavController().navigate(action)
                }
            }
        })
    }

    private fun initRecyclerView() {
        val listener: RecyclerViewClickListener = object : RecyclerViewClickListener {
            override fun onClick(position: Int) {
                viewModel.onItemClick(position)
            }
        }
        val movieAdapter = MovieListAdapter(listener)
        binding.movieList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = movieAdapter
            scrollListener.vm = viewModel
            addOnScrollListener(scrollListener)
        }
        viewModel.movieList.observe(this, Observer {
            movieAdapter.submitList(it.items)
            scrollListener.hasMorePages = it.hasMorePages
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.movie_list_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.movie_list_filter) {
            val newFragment = DatePickerFragment()
            newFragment.show(childFragmentManager, "datePicker")
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun dateSelected(year: Int, month: Int, day: Int) {
        viewModel.setDateFilter(year, month, day)
    }

    override fun onReset() {
        viewModel.resetDateFilter()
    }
}

class PagingListener(var hasMorePages: Boolean = true, var vm: MovieListViewModel? = null) : RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val llm = recyclerView.layoutManager as LinearLayoutManager
        Timber.v("visible=%d; total=%d; first=%d; last=%s", llm.childCount, llm.itemCount, llm.findFirstVisibleItemPosition(), llm.findLastVisibleItemPosition())
        if (hasMorePages && llm.itemCount < llm.findLastVisibleItemPosition() + 10) {
            vm?.loadNextPage()
        }
    }
}

