package de.alosdev.moviedb.movie.detail.data

data class Movie(
    val id: Int,
    val posterPath: String? = null,
    val title: String,
    val backdropPath: String? = null,
    val overview: String
)
