package de.alosdev.moviedb.movie.detail.repository

import androidx.lifecycle.MutableLiveData
import de.alosdev.moviedb.config.repository.ConfigRepository
import de.alosdev.moviedb.core.coroutine.AppCoroutineDispatchers
import de.alosdev.moviedb.http.data.Failure
import de.alosdev.moviedb.http.data.Loading
import de.alosdev.moviedb.http.data.Resource
import de.alosdev.moviedb.http.data.Success
import de.alosdev.moviedb.movie.detail.api.MovieDetailApi
import de.alosdev.moviedb.movie.detail.data.Movie
import de.alosdev.moviedb.movie.detail.data.mapper.MovieMapper
import kotlinx.coroutines.experimental.withContext
import timber.log.Timber

class MovieDetailRepository(
    private val dispatchers: AppCoroutineDispatchers,
    private val api: MovieDetailApi,
    private val configRepository: ConfigRepository,
    private val mapper: MovieMapper,
    private val movieId: Int
) {
    val movieResource = MutableLiveData<Resource<Movie>>()

    suspend fun loadMovie() {
        movieResource.postValue(Loading)
        withContext(dispatchers.network) {
            try {
                val imageConfig = configRepository.fetchImageConfig()
                val response = api.fetchMovie(movieId).await()
                if (response.isSuccessful) {
                    movieResource.postValue(
                        Success(
                            mapper.map(
                                imageConfig = imageConfig,
                                input = response.body()!!
                            )
                        )
                    )
                } else {
                    movieResource.postValue(Failure(response.code(), response.message()))
                }
            } catch (exception: Exception) {
                Timber.e(exception, "cannot load movie list")
                movieResource.postValue(Failure(-1, "cannot load movie list"))
            }
        }
    }
}
