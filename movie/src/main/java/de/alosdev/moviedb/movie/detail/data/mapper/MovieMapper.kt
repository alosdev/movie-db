package de.alosdev.moviedb.movie.detail.data.mapper

import de.alosdev.moviedb.config.data.ImageConfig
import de.alosdev.moviedb.movie.detail.api.response.MovieResponse
import de.alosdev.moviedb.movie.detail.data.Movie

class MovieMapper {
    fun map(
        imageConfig: ImageConfig,
        input: MovieResponse
    ): Movie {
        return Movie(
            id = input.id,
            title = input.title,
            posterPath = if (input.posterPath == null) null else imageConfig.createPosterImage(input.posterPath),
            backdropPath = if (input.backdropPath == null) null else imageConfig.createBackdropImage(input.backdropPath),
            overview = input.overview
        )
    }
}
