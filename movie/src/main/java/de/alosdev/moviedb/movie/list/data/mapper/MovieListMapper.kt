package de.alosdev.moviedb.movie.list.data.mapper

import de.alosdev.moviedb.config.data.ImageConfig
import de.alosdev.moviedb.movie.list.api.response.MoviePageResponse
import de.alosdev.moviedb.movie.list.data.Movie
import de.alosdev.moviedb.movie.list.data.MovieList

class MovieListMapper {
    fun map(
        imageConfig: ImageConfig,
        pageResponse: MoviePageResponse
    ): MovieList {
        return MovieList(
            currentPage = pageResponse.page,
            totalPages = pageResponse.totalPages,
            items = pageResponse.results.map { movieResponse ->
                Movie(
                    id = movieResponse.id,
                    title = movieResponse.title,
                    posterPath = if (movieResponse.posterPath == null) null else imageConfig.createPosterImage(movieResponse.posterPath)
                )
            }
        )
    }
}
