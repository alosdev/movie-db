package de.alosdev.moviedb.http.interceptor

import com.google.common.truth.Truth.assertThat
import de.alosdev.moviedb.http.BuildConfig
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AuthenticationInterceptorTest {
    private lateinit var toBeTested: AuthenticationInterceptor
    @get:Rule
    val server = MockWebServer()

    private lateinit var client: OkHttpClient
    private lateinit var host: String
    private lateinit var url: HttpUrl
    @Before
    fun setUp() {
        toBeTested = AuthenticationInterceptor()
        client = OkHttpClient.Builder()
            .addInterceptor(toBeTested)
            .build()

        host = server.hostName + ":" + server.port
        url = server.url("/")
    }

    @Test
    fun checksSettingCorrectValue() {
        server.enqueue(MockResponse())
        val response = client.newCall(request().build()).execute()
        assertThat(response.request().url().queryParameter("api_key")).matches(BuildConfig.API_KEY)
        response.body()?.close()
    }

    @Test
    fun checksOverrideCorrectValue() {
        server.enqueue(MockResponse())
        val response = client.newCall(
            Request.Builder().url(
                url.newBuilder().setQueryParameter("api_key", "helloWorld").build()
            ).build()
        ).execute()
        assertThat(response.request().url().queryParameter("api_key")).matches(BuildConfig.API_KEY)
        response.body()?.close()
    }

    private fun request(): Request.Builder {
        return Request.Builder().url(url)
    }
}
