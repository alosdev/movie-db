package de.alosdev.moviedb.http

import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import de.alosdev.moviedb.http.interceptor.AuthenticationInterceptor
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.Module
import org.koin.dsl.module.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

internal val HTTP_LOGGING_LEVEL = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

val httpModule: Module = module {
    factory {
        CoroutineCallAdapterFactory() as CallAdapter.Factory
    }
    factory {
        MoshiConverterFactory.create() as Converter.Factory
    }
    factory {
        Retrofit.Builder()
            .addCallAdapterFactory(get())
            .addConverterFactory(get())
            .baseUrl(BuildConfig.API_BASE_URL)
            .client(get())
            .build()
    }
    factory {
        createOkHttpClient(
            authenticationInterceptor = get(),
            context = get()
        )
    }
    factory {
        AuthenticationInterceptor() as Interceptor
    }

}

fun createOkHttpClient(authenticationInterceptor: Interceptor, context: Context): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HTTP_LOGGING_LEVEL

    return OkHttpClient.Builder()
        .addInterceptor(httpLoggingInterceptor)
        .addInterceptor(authenticationInterceptor)
        .cache(Cache(context.cacheDir, 20 * 1024 * 1024L)) // 20 MB cache
        .build()
}
