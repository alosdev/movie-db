package de.alosdev.moviedb.http.interceptor

import de.alosdev.moviedb.http.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class AuthenticationInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val oldRequest = chain.request()
        val httpUrl = oldRequest.url()
            .newBuilder()
            .setEncodedQueryParameter("api_key", BuildConfig.API_KEY).build()
        val requestWithAuth = oldRequest.newBuilder().url(httpUrl)
            .build()
        return chain.proceed(requestWithAuth)
    }
}
