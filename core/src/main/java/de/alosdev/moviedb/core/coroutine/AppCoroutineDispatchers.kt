package de.alosdev.moviedb.core.coroutine

import kotlin.coroutines.experimental.CoroutineContext

data class AppCoroutineDispatchers(
    val network: CoroutineContext,
    val main: CoroutineContext,
    val computation: CoroutineContext
)
