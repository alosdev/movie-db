package de.alosdev.moviedb.core.ui

import androidx.lifecycle.ViewModel
import de.alosdev.moviedb.core.coroutine.AppCoroutineDispatchers
import kotlinx.coroutines.experimental.CompletionHandler
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.Job

abstract class BaseViewModel(private val dispatchers: AppCoroutineDispatchers) : ViewModel() {
    private var jobs = listOf<Job>()

    fun launch(code: suspend CoroutineScope.() -> Unit) {
        jobs += kotlinx.coroutines.experimental.launch(dispatchers.main, block = code)
    }

    /**
     * Called when the view is ready to start
     */
    open fun start() {}

    override fun onCleared() {
        super.onCleared()
        jobs.forEach { it.cancel() }
    }
}
