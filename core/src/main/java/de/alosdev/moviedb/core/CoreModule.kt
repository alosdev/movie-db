package de.alosdev.moviedb.core

import de.alosdev.moviedb.core.coroutine.AppCoroutineDispatchers
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.newSingleThreadContext
import org.koin.dsl.module.module

val coreModule = module {
    single {
        AppCoroutineDispatchers(
            main = UI,
            network = CommonPool,
            computation = CommonPool
        )
    }
}
