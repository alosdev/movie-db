package de.alosdev.moviedb.core.ui.mvvm

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("goneUnless")
fun goneUnless(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView, url: String?) {
    if (!url.isNullOrEmpty()) {
        Picasso.get()
            .load(url)
            .into(imageView)
        imageView.scaleType=ImageView.ScaleType.CENTER_CROP
    } else {
        imageView.setImageResource(android.R.drawable.star_on)
        imageView.scaleType = ImageView.ScaleType.FIT_CENTER
    }
}
