package de.alosdev.moviedb.core.ui.mvvm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import de.alosdev.moviedb.core.BR
import de.alosdev.moviedb.core.ui.BaseViewModel
import org.koin.androidx.viewmodel.ext.android.viewModelByClass
import org.koin.core.parameter.ParameterDefinition
import org.koin.core.parameter.emptyParameterDefinition
import kotlin.reflect.KClass

abstract class MvvmFragment<VB : ViewDataBinding, VM : BaseViewModel>(clazz: KClass<VM>) : Fragment() {

    lateinit var binding: VB
    val viewModel: VM by viewModelByClass(clazz = clazz, parameters = this.getParameters())

    protected open fun getParameters(): ParameterDefinition {
        return emptyParameterDefinition()
    }

    @CallSuper
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        binding.setLifecycleOwner(this)
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root
    }

    @CallSuper
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.start()
    }

    abstract fun getLayoutId(): Int
}
