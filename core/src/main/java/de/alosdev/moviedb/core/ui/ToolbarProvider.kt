package de.alosdev.moviedb.core.ui

import androidx.appcompat.widget.Toolbar

interface ToolbarProvider {
    fun getToolbar(): Toolbar
}
