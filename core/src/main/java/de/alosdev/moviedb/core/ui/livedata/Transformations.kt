package de.alosdev.moviedb.core.ui.livedata

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

@MainThread
fun <X, Y> map(
    source: LiveData<X>,
    predicate: (X) -> Boolean = { true },
    mapFunction: (X) -> Y
): LiveData<Y> {
    val result = MediatorLiveData<Y>()
    result.addSource(source) { x ->
        if (predicate.invoke(x)) {
            result.value = mapFunction.invoke(x)
        }
    }
    return result
}
